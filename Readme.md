#CA assignment
####Tommy Kang - s00141425

![done](http://www.milkshakedesign.com/wp-content/uploads/2014/01/what-weve-done.png)

#####1. created initial BitBucket
#####18/Mar/2016

#####1. created three tables (continent, country and city)
#####2. added a business layer with Entity Framework Code First
#####3. made geneic repository interface and linked with Ninject mvc5
#####4. generated DTO with Automapper to push only available json data
#####19/Mar/2016

#####1. ajax with local database
#####22/Mar/2016

#####1. tried to link public Web API to own API
#####23/Mar/2016

#####1. do further link Web api to AJAX
#####24/Mar/2016

#####1. fixed between WebAPI and Ajax
#####2. added setTimeout to solve multiple Ajax calls
#####25/Mar/2016

![done](./webapi.png)