using System.Collections.Generic;
using System.Data.Entity.Migrations;
using BusinessLayer.Models;

namespace BusinessLayer.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CityModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(CityModel context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            List<Continent> continents = new List<Continent>();

            Continent europe = new Continent()
            {
                ContinentName = "Europe",
                Countries = new List<Country>()
                {
                  new Country
                  {
                      CountryName = "Ireland",
                      Cities = new List<City>()
                      {
                          new City() { CityName = "Dublin"},
                          new City() { CityName = "Cork"},
                          new City() { CityName = "Galway"},
                          new City() { CityName = "Castlebar"},
                          new City() { CityName = "Sligo"},
                          new City() { CityName = "Limerick"},
                          new City() { CityName = "Killenny"},
                          new City() { CityName = "Waterford"},
                          new City() { CityName = "Drogheda"},
                          new City() { CityName = "Dundalk"},
                          new City() { CityName = "Swords"}
                      }
                  },
                  new Country()
                  {
                      CountryName = "United Kingdom",
                      Cities = new List<City>()
                      {
                          new City() {CityName = "Aberdeen"},
                          new City() {CityName = "Birminghan"},
                          new City() {CityName = "Bradford"},
                          new City() {CityName = "Bristol"},
                          new City() {CityName = "Cambridge"},
                          new City() {CityName = "Canterbury"},
                          new City() {CityName = "Cardiff"},
                          new City() {CityName = "London"}
                      }
                  },
                  new Country()
                  {
                      CountryName = "France",
                      Cities = new List<City>()
                      {
                          new City() {CityName = "Paris"},
                          new City() {CityName = "Marseille"},
                          new City() {CityName = "Lyon"},
                          new City() {CityName = "Toulouse"},
                          new City() {CityName = "Nice"},
                          new City() {CityName = "Nantes"},
                          new City() {CityName = "Strasbourg"},
                          new City() {CityName = "Montpellier"}
                      }
                  },
                  new Country()
                  {
                      CountryName = "German",
                      Cities = new List<City>()
                      {
                          new City() {CityName = "Berlin"},
                          new City() {CityName = "Hamburg"},
                          new City() {CityName = "Munich"},
                          new City() {CityName = "Cologne"},
                          new City() {CityName = "Frankfurt"},
                          new City() {CityName = "Essen"}
                      }
                  }
                }
            };

            continents.Add(europe);

            Continent asia = new Continent()
            {
                ContinentName = "Asia",
                Countries = new List<Country>()
                {
                  new Country
                  {
                      CountryName = "South Korea",
                      Cities = new List<City>()
                      {
                          new City() { CityName = "Seoul"},
                          new City() { CityName = "Busan"},
                          new City() { CityName = "Incheon"},
                          new City() { CityName = "Daegu"}
                      }
                  },
                  new Country()
                  {
                      CountryName = "China",
                      Cities = new List<City>()
                      {
                          new City() {CityName = "Shanghai"},
                          new City() {CityName = "Beijing"},
                          new City() {CityName = "Guangzhou"},
                          new City() {CityName = "Shenzhen"}
                      }
                  },
                  new Country()
                  {
                      CountryName = "Japan",
                      Cities = new List<City>()
                      {
                          new City() {CityName = "Tokyo"},
                          new City() {CityName = "Kanagawa"},
                          new City() {CityName = "Osaka"},
                          new City() {CityName = "Kyoto"},
                      }
                  },
                  new Country()
                  {
                      CountryName = "India",
                      Cities = new List<City>()
                      {
                          new City() {CityName = "Mumbai"},
                          new City() {CityName = "Kolkata"},
                          new City() {CityName = "Delhi"},
                          new City() {CityName = "Chennai"}
                      }
                  }
                }
            };

            continents.Add(asia);

            Continent africa = new Continent()
            {
                ContinentName = "Africa",
                Countries = new List<Country>()
                {
                    new Country()
                    {
                        CountryName = "Nigeria",
                        Cities = new List<City>()
                        {
                            new City() {CityName = "Lagos"}
                        }
                    },
                    new Country()
                    {
                        CountryName = "Egypt",
                        Cities = new List<City>()
                        {
                            new City() {CityName = "Cariro"}
                        }
                    },
                    new Country()
                    {
                        CountryName = "Congo",
                        Cities = new List<City>()
                        {
                            new City() {CityName = "Kinshasa"}
                        }
                    }
                }
            };

            continents.Add(africa);

            Continent america = new Continent()
            {
                ContinentName = "America",
                Countries = new List<Country>()
                {
                    new Country()
                    {
                        CountryName = "Mexico",
                        Cities = new List<City>()
                        {
                            new City() {CityName = "Mexico City"}
                        }
                    },
                    new Country()
                    {
                        CountryName = "Brazil",
                        Cities = new List<City>()
                        {
                            new City() {CityName = "Sao Paulo"}
                        }
                    },
                    new Country()
                    {
                        CountryName = "United State",
                        Cities = new List<City>()
                        {
                            new City() {CityName = "New York"}
                        }
                    },
                    new Country()
                    {
                        CountryName = "Argentina",
                        Cities = new List<City>()
                        {
                            new City() {CityName = "Buenos Aires"}
                        }
                    }
                }
            };

            continents.Add(america);

            continents.ForEach(cont=>context.Continents.Add(cont));
        }
    }
}
