using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace BusinessLayer.Models
{
    public class CityModel : DbContext
    {
        // Your context has been configured to use a 'CityModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'WebAPI.Models.CityModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CityModel' 
        // connection string in the application configuration file.
        public CityModel()
            : base("name=CityModel")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Continent> Continents { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<City> Cities { get; set; }

    }

    public class Continent
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DisplayName("Continent Name")]
        public string ContinentName { get; set; }

        public ICollection<Country> Countries { get; set; }
    }

    public class Country
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DisplayName("Country Name")]
        public string CountryName { get; set; }

        public int ContinentId { get; set; }
        [ForeignKey("ContinentId")]
        public Continent Continent { get; set; }
        
        public ICollection<City> Cities { get; set; }
    }

    public class City
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DisplayName("City Name")]
        public string CityName { get; set; }

        public int CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
    }
}