﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models.DTO
{
    public class CityDTO
    {
        public int Id { get; set; }
        public string CityName { get; set; }
    }
}
