﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models.DTO
{
    public class ContinentDTO
    {
        public int Id { get; set; }
        public string ContinentName { get; set; }
        public ICollection<CountryDTO> Countries { get; set; }
    }
}
