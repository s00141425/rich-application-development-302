﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.Models;
using BusinessLayer.Models.DTO;

namespace BusinessLayer.DAL
{
    public interface IContinentRepository : IRepository<ContinentDTO>
    {
    }

    public class ContinentRepository : IContinentRepository
    {
        private CityModel _db;
        private IMapper mapper;

        public ContinentRepository(CityModel db)
        {
            this._db = db;
            mapper = AutomapperConfiguration.MapperConfiguration.CreateMapper();
        }

        public void Dispose()
        {
            this._db.Dispose();
        }

        public ICollection<ContinentDTO> GetAllItems()
        {
            var continents = _db.Continents
                .Include(cont => cont.Countries.Select(c => c.Cities));

            return mapper.Map<List<ContinentDTO>>(continents);
        }

        public ContinentDTO GetItemById(int? id)
        {
            var continet = _db.Continents
                .Include(cont => cont.Countries.Select(c => c.Cities)).FirstOrDefault(x => x.Id == id);
            return mapper.Map<ContinentDTO>(continet);
        }
    }
}
