﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.Models;
using BusinessLayer.Models.DTO;

namespace BusinessLayer.DAL
{
    public interface ICityRepository: IRepository<CityDTO>
    {
    }

    public class CityRepository : ICityRepository
    {
        private CityModel _db;
        private IMapper mapper;

        public CityRepository(CityModel db)
        {
            this._db = db;
            mapper = AutomapperConfiguration.MapperConfiguration.CreateMapper();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public ICollection<CityDTO> GetAllItems()
        {
            var cities = _db.Cities;
            return mapper.Map<List<CityDTO>>(cities);
        }

        public CityDTO GetItemById(int? id)
        {
            var city = _db.Cities.Find(id);
            return mapper.Map<CityDTO>(city);
        }
    }
}
