﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DAL
{
    public interface IRepository<T> : IDisposable
    {
        ICollection<T> GetAllItems();
        T GetItemById(int? id);
    }
}
