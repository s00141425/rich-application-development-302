﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.Models;
using BusinessLayer.Models.DTO;

namespace BusinessLayer.DAL
{
    public static class AutomapperConfiguration
    {
        public static MapperConfiguration MapperConfiguration { get; set; }

        public static void Configure()
        {
            MapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Continent, ContinentDTO>();
                cfg.CreateMap<Country, CountryDTO>();
                cfg.CreateMap<City, CityDTO>();
            });
        }
    }
}
