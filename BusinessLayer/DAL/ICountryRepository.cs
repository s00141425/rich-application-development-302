﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using BusinessLayer.Models;
using BusinessLayer.Models.DTO;

namespace BusinessLayer.DAL
{
    public interface ICountryRepository : IRepository<CountryDTO>
    {
    }

    public class CountryRepository : ICountryRepository
    {
        private CityModel _db;
        private IMapper mapper;

        public CountryRepository(CityModel db)
        {
            this._db = db;
            mapper = AutomapperConfiguration.MapperConfiguration.CreateMapper();
        }

        public void Dispose()
        {
            this._db.Dispose();
        }

        public ICollection<CountryDTO> GetAllItems()
        {
            var country = _db.Countries.Include(cout=>cout.Cities);

            return mapper.Map<List<CountryDTO>>(country);
        }

        public CountryDTO GetItemById(int? id)
        {
            var country = _db.Countries.Include(cout => cout.Cities).FirstOrDefault(city => city.Id == id);
            return mapper.Map<CountryDTO>(country);
        }
    }
}
